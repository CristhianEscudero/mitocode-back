package com.back.service;

import java.time.LocalDateTime;
import java.util.List;

import com.back.dto.ConsultaListaExamenDTO;
import com.back.dto.ConsultaResumenDTO;
import com.back.dto.FiltroConsultaDTO;
import com.back.model.Consulta;

public interface IConsultaService extends ICrud<Consulta, Integer>{

	Consulta registrarTransaccional(ConsultaListaExamenDTO dto) throws Exception;
	
	List<Consulta> buscar(FiltroConsultaDTO filtro);

	List<Consulta> buscarFecha(FiltroConsultaDTO filtro, LocalDateTime fechaFin);
	
	List<ConsultaResumenDTO> listarResumen();
	
	byte[] generarReporte();
	
}
