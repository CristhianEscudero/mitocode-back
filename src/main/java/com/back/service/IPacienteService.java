package com.back.service;

import com.back.model.Paciente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IPacienteService extends ICrud<Paciente, Integer>{

	Page<Paciente> listarPageable(Pageable pageable);
	
}
