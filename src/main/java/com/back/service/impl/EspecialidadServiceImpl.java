package com.back.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.back.model.Especialidad;
import com.back.repo.IEspecialidadRepo;
import com.back.repo.IGenericRepo;
import com.back.service.IEspecialidadService;

@Service
public class EspecialidadServiceImpl extends CrudImpl<Especialidad, Integer> implements IEspecialidadService{
	
	@Autowired
	private IEspecialidadRepo repo;
	
	
	@Override
	protected IGenericRepo<Especialidad, Integer> getRepo(){
		return repo;
	}
	
	
}
