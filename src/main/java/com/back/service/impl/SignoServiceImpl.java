package com.back.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.back.model.Signo;
import com.back.repo.IGenericRepo;
import com.back.repo.ISignoRepo;
import com.back.service.ISignoService;


@Service
public class SignoServiceImpl extends CrudImpl<Signo, Integer> implements ISignoService{
	
	@Autowired
	private ISignoRepo repo;

	@Override
	protected IGenericRepo<Signo, Integer> getRepo() {
		return repo;
	}


	
}
