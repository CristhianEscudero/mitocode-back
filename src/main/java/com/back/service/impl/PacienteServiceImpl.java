package com.back.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.back.model.Paciente;
import com.back.repo.IGenericRepo;
import com.back.repo.IPacienteRepo;
import com.back.service.IPacienteService;

@Service
public class PacienteServiceImpl extends CrudImpl<Paciente, Integer> implements IPacienteService{
	
	@Autowired
	private IPacienteRepo repo;
	
	
	@Override
	protected IGenericRepo<Paciente, Integer> getRepo(){
		return repo;
	}
	
	@Override
	public Page<Paciente> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}
	
}
