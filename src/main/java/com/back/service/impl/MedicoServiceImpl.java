package com.back.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.back.model.Medico;
import com.back.repo.IGenericRepo;
import com.back.repo.IMedicoRepo;
import com.back.service.IMedicoService;

@Service
public class MedicoServiceImpl extends CrudImpl<Medico, Integer> implements IMedicoService{
	
	@Autowired
	private IMedicoRepo repo;
	
	
	@Override
	protected IGenericRepo<Medico, Integer> getRepo(){
		return repo;
	}
	
	
}
