package com.back.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.back.model.Examen;
import com.back.repo.IExamenRepo;
import com.back.repo.IGenericRepo;
import com.back.service.IExamenService;

@Service
public class ExamenServiceImpl extends CrudImpl<Examen, Integer> implements IExamenService{
	
	@Autowired
	private IExamenRepo repo;

	@Override
	protected IGenericRepo<Examen, Integer> getRepo(){
		return repo;
	}

	
}
