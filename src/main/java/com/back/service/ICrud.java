package com.back.service;

import java.util.List;

public interface ICrud<T, ID> {

	T registrar(T pac) throws Exception;
	T modificar(T pac) throws Exception;
	List<T> listar() throws Exception;
	T ListarxId(ID id) throws Exception;
	void eliminar(ID id) throws Exception;
	
}
