package com.back.service;

import java.util.List;

import com.back.model.Menu;

public interface IMenuService extends ICrud<Menu, Integer>{
	
	List<Menu> listarMenuPorUsuario(String nombre);

}
