package com.back.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.back.exception.ModeloNotFoundException; 
import com.back.model.Signo;
import com.back.service.ISignoService;

@RestController
@RequestMapping("/signos")
public class SignoController {
	@Autowired
	private ISignoService service;

	@PreAuthorize("@authServiceImpl.tieneAcceso('listar')")
	@GetMapping
	public ResponseEntity<List<Signo>> listar() throws Exception {
		List<Signo> lista = service.listar();
		return new ResponseEntity<>(lista, HttpStatus.OK);

	}

	@GetMapping("/hateoas/{id}")
	public EntityModel<Signo> listarPorIdHateoas(@PathVariable("id") Integer id) throws Exception {
		Signo obj = service.ListarxId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
		}

		EntityModel<Signo> recurso = EntityModel.of(obj);
		WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarPorId(id));

		recurso.add(linkTo.withRel("Medico-recurso"));

		return recurso;
	}

	@GetMapping("/{id}")
	public ResponseEntity<Signo> listarPorId(@Valid @PathVariable("id") Integer id) throws Exception {
		Signo obj = service.ListarxId(id);
		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
		}
		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Signo> registrar(@Valid @RequestBody Signo signo) throws Exception {
		Signo obj = service.registrar(signo);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(obj.getIdSigno()).toUri();

		return ResponseEntity.created(location).build();
		
	}

	@PutMapping
	public ResponseEntity<Signo> modificar(@Valid @RequestBody Signo signo) throws Exception {
		Signo obj = service.modificar(signo);
		return new ResponseEntity<Signo>(obj, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		Signo obj = service.ListarxId(id);
		if (obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO: " + id);
		}
		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}


}
